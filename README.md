# Magento Custom Navigation #
![eurostep.png](https://bitbucket.org/repo/KEMK9q/images/1373744460-eurostep.png)
## Simple module for custom Magento's menu navigation management ##
This module provide a simple way to add custom menu voices to Magento. This module make easy to compose Magento navigation with categories, subcategories, simple links and custom html blocks.