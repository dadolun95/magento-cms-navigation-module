<?php

/**
 * Catalog Observer
 *
 * @category   Mage
 * @package    Eurostep_Navigation
 * @author     Davide <davide.lunardon@eurostep.it>
 */
class Eurostep_Navigation_Adminhtml_NavigationbackendController extends Mage_Adminhtml_Controller_Action
{

	 /**
     * Load Navigation Manager admin layout
     */
	public function indexAction()
    {
       $this->loadLayout();
	   $this->_title($this->__("Navigation Manager"));
	   $this->renderLayout();
    }

     /**
     * Save custom Navigation on db
     */
    public function saveNavigationAction(){

    	$navigation = Mage::app()->getRequest()->getParam('navigation');
	    $navigation_table = Mage::getSingleton('core/resource')->getTableName('eurostep_navigation');

		$dbRead = Mage::getSingleton('core/resource')->getConnection('core_read');
		$query = "SELECT id FROM $navigation_table";
		$data = $dbRead->query($query);
		$ids = [];
		foreach($data->fetchAll() as $id){
			$ids[] = $id["id"];
		}

    	try {

			$dbWrite = Mage::getSingleton('core/resource')->getConnection('core_write');

			$fields = array();
			$count = 0;

			$navigation = json_decode($navigation);

			foreach($navigation as $voice){

				$fields['id'] = $voice->id;
				$fields['name'] = $voice->name;
				$fields['alias'] = $voice->name;
				$fields['position'] = $voice->order;
				$fields['inner_class'] = $voice->innerClass;
				$fields['outer_class'] = $voice->outerClass;
				if(isset($voice->catid)){
					$fields['catid'] = $voice->catid;
				} else {
					$fields['catid'] = 0;
				}
				if(isset($voice->block)){
					$fields['block'] = $voice->block;
				} else {
					$fields['block'] = 0;
				}
				if(isset($voice->url)){
					$fields['url'] = $voice->url;
					$fields['url_target'] = $voice->url_target;
				} else {
					$fields['url'] = "";
					$fields['url_target'] = "";
				}
				$fields['store_id'] = $voice->store;

				if(!in_array($voice->id,$ids)){

					$fields["id"] = "";

					if(!isset($voice->delete)){

						$dbWrite->insert($navigation_table, $fields);

					}

				} else {


					if(isset($voice->delete)){
						$query = "DELETE FROM $navigation_table WHERE id = ".$fields["id"];
					} else {

						$query = "UPDATE $navigation_table SET name='".$fields["name"]."', alias='".$fields["alias"]."', position=".$fields["position"].", catid=".$fields["catid"].", block=".$fields["block"].", url='".$fields["url"]."', inner_class='".$fields["inner_class"]."', outer_class='".$fields["outer_class"]."', store_id=".$fields["store_id"].", url_target='".$fields["url_target"]."' WHERE id = ".$fields["id"];
					}

					$dbWrite->query($query);

				}

			}

			return true;
		} catch (Exception $e){
			echo $e;
			return false;
		}
    }
}