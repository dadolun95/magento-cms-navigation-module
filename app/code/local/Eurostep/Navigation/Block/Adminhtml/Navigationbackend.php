<?php  

/**
 * Navigation Module Block
 *
 * @category    Mage
 * @package     Eurostep_Navigation
 * @author      Davide <davide.lunardon@eurostep.it>
 */
class Eurostep_Navigation_Block_Adminhtml_Navigationbackend extends Mage_Adminhtml_Block_Template {

	public function renderStoreSelect(){

		$html = "<select name='storeId' id='navigation-store-id'>";
		foreach ($this->getStores() as $store) {
			$html .="<option value='".$store[1]."'>".$store[0]."</option>";
		}
		return $html."\n\r</select>";
	}

	protected function getStores(){
		$stores = array();
		foreach (Mage::app()->getWebsites() as $website) {
		    foreach ($website->getGroups() as $group) {
		    	foreach($group->getStores() as $store) {
		    		$stores[$website->getName()] = array($store->getName(),$store->getId());
		    	}
		    }
		}
		return $stores;
	}

	protected function getCategoryThree($storeId){
	
        $parent = Mage::app()->getStore($storeId)->getRootCategoryId();

        $storeCategories = array();
        $this->getCategoryChildrens($parent, 0, $storeCategories);

        return $storeCategories;

	}

	protected function getCategoryChildrens($categoryId, $level, &$storeCategories){

		$category = Mage::getModel('catalog/category')->load($categoryId);

		if($category->getIsActive() && $category->getIncludeInMenu()){
			if($level != 0){
				$storeCategories[] = array("id" => $categoryId, "level" => $level, "parent" => $category->getParentId(), "name" => $category->getName(), "url" => $category->getURL());
			}
	        
	        if (!$category->checkId($categoryId)) {
	            if ($asCollection) {
	                return new Varien_Data_Collection();
	            }
	            return array();
	        }

	        if($level < 1 && $category->hasChildren()){

				foreach(explode(',',$category->getChildren()) as $subCatid)
				{
					$this->getCategoryChildrens($subCatid, $level+1, $storeCategories);
				}
			}
		}

		return;

	}

    protected function getStoreNavigation($storeId){

    	$navigation_table = Mage::getSingleton('core/resource')->getTableName('eurostep_navigation');
		$db = Mage::getSingleton('core/resource')->getConnection('core_read');

		$query = "SELECT * FROM $navigation_table WHERE store_id = $storeId ORDER BY position";

		$data = $db->query($query);

		return $data->fetchAll();

    }

    protected function renderNavigationHtml($storeMenu,$storeId){

    	foreach($storeMenu as $voice) {
    		if($voice["catid"]){
    			$html .= "<tr id='".$voice["id"]."' class='level-1 saved store-".$storeId."' catid='".$voice["catid"]."'><td><input type='text' value='".$voice["name"]."' class='voice-name' id='".$voice["id"]."'></td><td><p>Id categoria = ".$voice["catid"]."</p></td>";
    		} elseif($voice["block"]){
    			$html .= "<tr id='".$voice["id"]."' class='level-1 saved store-".$storeId."' block='".$voice["block"]."'><td><input type='text' value='".$voice["name"]."' class='voice-name' id='".$voice["id"]."'></td><td>".$this->renderStaticBlocksSelector("block-select-".$voice["id"], "value-changer", $voice["block"])."</td>";
    		} elseif(!is_null($voice["url"])){
    			$html .= "<tr id='".$voice["id"]."' class='level-1 saved store-".$storeId."' url='".$voice["url"]."'><td><input type='text' value='".$voice["name"]."' class='voice-name' id='".$voice["id"]."'></td><td><input type='text' class='value-changer' value='".$voice["url"]."'><select class='value-changer url-target'>
						<option value='_blank' ".(($voice["url_target"] == "_blank")?"selected":"").">_blank</option>
						<option value='_self' ".(($voice["url_target"] == "_self")?"selected":"").">_self</option>
					</select></td>";
    		}
    		$html.="<td><input type='text' class='outer-class-input' value='".$voice["outer_class"]."'></td><td><input type='text' class='inner-class-input' value='".$voice["inner_class"]."'></td><td><input type='number' min='0' class='order-input' value='".$voice["position"]."'></td><td><button title='Delete' type='button' voice='".$voice["id"]."' class='scalable task delete-voice'><span><span><span>Cancella</span></span></span></button></td>";
    		$html.="</tr>";
    	}

    	return $html;

    }

    public function renderNavigation(){

    	$stores = $this->getStores();

		$hmtl = "";

		$first = true;

		foreach($stores as $store){

			$html.= '
			<table cellspacing="0" '.(($first)?'style="display:block;"':'style="display:none;"').' class="navigation-data" id="navigation-table-'.$store[1].'">
				<colgroup>
					<col width="500" class="a-center">
	                <col width="500" class="a-center">
	                <col width="500" class="a-center">
	                <col width="500" class="a-center">
	                <col width="500" class="a-center">
	                <col width="500" class="a-center">
                </colgroup>
				<thead>
					<tr class="headings">
						<th><span class="nobr">Nome</span></th>
			            <th><span class="nobr">Valore</span></th>
			            <th><span class="nobr">Classe label</span></th>
			            <th><span class="nobr">Classe tendina</span></th>
			            <th><span class="nobr">Ordine</span></th>
			            <th><span class="nobr">&nbsp;</span></th>
			        </tr>
			    </thead>';

			$html .= "<tbody id='navigation-content-manager-".$store[1]."'>";

			$storeMenu = $this->getStoreNavigation($store[1]);

			$voiceIds = array();

			foreach ($storeMenu as $voice) {
				array_push($voiceIds,$voice["catid"]);
			}

			foreach($this->getCategoryThree($store[1]) as $cat){
				if(!in_array($cat["id"],$voiceIds)) {
					$cat["catid"] = $cat["id"];
					$cat["id"] = "new-cat-".$cat["id"];
					$storeMenu[] = $cat;
				}
			}

			if(count($storeMenu)){

				$html .= $this->renderNavigationHtml($storeMenu,$store[1]);

			}

			$html .= "</tbody></table>";
			$first = false;
	            
	    }

        return $html;
    }

    public function renderStaticBlocksSelector($tagId = "static-block-select", $class = "", $selected = 0){

    	$blocks = Mage::getResourceModel('cms/block_collection')->load()->toOptionArray();

    	$html = "<select name='static-block' class='".$class."' id='".$tagId."'>";

    	foreach($blocks as $block){
    		$html .= "<option value='".$block["value"]."'".(($selected == $block["value"])?" selected ":"").">".$block["label"]."</option>";
    	}

    	$html .= "</select>";

    	return $html;

    }

}