<?php

/**
 * Top menu block Override
 *
 * @category    Mage
 * @package     Eurostep_Navigation
 * @author      Davide <davide.lunardon@eurostep.it>
 */
class Eurostep_Navigation_Block_Page_Html_Topmenu_Navigation extends Mage_Page_Block_Html_Topmenu
{

    /**
     * Get top menu html
     *
     * @param string $outermostClass
     * @param string $childrenWrapClass
     * @return string
     */
    public function getHtml($outermostClass = '', $childrenWrapClass = '')
    {

        //Call Catalog Observer, populate navigation menu
        Mage::dispatchEvent('page_block_html_topmenu_gethtml_before', array(
            'menu' => $this->_menu,
            'block' => $this
        ));

        $this->_menu->setOutermostClass($outermostClass);
        $this->_menu->setChildrenWrapClass($childrenWrapClass);

        $html = $this->_getHtml($this->_menu, $childrenWrapClass);

        Mage::dispatchEvent('page_block_html_topmenu_gethtml_after', array(
            'menu' => $this->_menu,
            'html' => $html
        ));

        return $html;
    }

    /**
     * Recursively generates top menu html from data that is specified in $menuTree
     *
     * @param Varien_Data_Tree_Node $menuTree
     * @param string $childrenWrapClass
     * @return string
     * @deprecated since 1.8.2.0 use child block catalog.topnav.renderer instead
     */
    protected function _getHtml(Varien_Data_Tree_Node $menuTree, $childrenWrapClass)
    {
        $html = '';

        $children = $menuTree->getChildren();
        $parentLevel = $menuTree->getLevel();
        $childLevel = is_null($parentLevel) ? 0 : $parentLevel + 1;

        $counter = 1;
        $childrenCount = $children->count();

        $parentPositionClass = $menuTree->getPositionClass();
        $itemPositionClassPrefix = $parentPositionClass ? $parentPositionClass . '-' : 'nav-';

        foreach ($children as $child) {

            $child->setLevel($childLevel);
            $child->setIsFirst($counter == 1);
            $child->setIsLast($counter == $childrenCount);
            $child->setPositionClass($itemPositionClassPrefix . $counter);

            $childData = $child->getData();

            $outerClasses = $childData["outerClasses"];
            $innerClasses = $childData["innerClasses"];

            $outermostClass = $outerClasses;

            if($parentLevel > 0){
                $outermostClass = $menuTree->getOutermostClass()." ".$outerClasses;
            } 

            if($parentLevel <= 0 && !$child->hasChildren()){
                 $outermostClass = 'level0 cursor-disabled '.$outerClasses;
            }

            if ($childLevel == 0 && $outermostClass) {
                $outermostClass = ' class="' . $outermostClass . '"';
            }

            $html .= '<li ' . $this->_getRenderedMenuItemAttributes($child) . '>';
            $html .= '<a href="' . $child->getUrl() . '" ' . $outermostClass . ' '.((isset($childData["urlTarget"]))?"target='".$childData["urlTarget"]."'":"target='_self'").'>'
                . $this->escapeHtml($child->getName()) . '</a>';

            if ($child->hasChildren()) {
                if (!empty($childrenWrapClass)) {
                    $html .= '<div class="' . $childrenWrapClass . '">';
                }
                $html .= '<ul class="level' . $childLevel . ' '.$innerClasses.'">';
                $html .= $this->_getHtml($child, $childrenWrapClass);
                $html .= '</ul>';

                if (!empty($childrenWrapClass)) {
                    $html .= '</div>';
                }
            } else { 

                if($childData["block_id"]){
                    $html .= '<ul class="'.$innerClasses.'" >'.Mage::app()->getLayout()->createBlock('cms/block')->setBlockId($childData["block_id"])->toHtml().'</ul>';
                }

            }   
            $html .= '</li>';

            $counter++;
        }

        return $html;
    }
    
}
