<?php

/**
 * Catalog Observer
 *
 * @category   Mage
 * @package    Eurostep_Navigation
 * @author     Davide <davide.lunardon@eurostep.it>
 */
class Eurostep_Navigation_Model_Catalog_Observer extends Mage_Catalog_Model_Observer
{
    /**
     * Adds catalog categories ad custom voices to top menu
     *
     * @param Varien_Event_Observer $observer
     */
    public function addCatalogToTopmenuItems(Varien_Event_Observer $observer)
    {
        $block = $observer->getEvent()->getBlock();
        $this->_addVoicesToMenu(
            $this->getCustomNavigation(Mage::app()->getStore()->getId()), $observer->getMenu(), $block
        );
    }

    /**
     * Get Navigation Menu's Data from DB
     *
     * @param int $storeId
     */
    protected function getCustomNavigation($storeId)
    {
        $navigation_table = Mage::getSingleton('core/resource')->getTableName('eurostep_navigation');
        $dbRead = Mage::getSingleton('core/resource')->getConnection('core_read');
        $query = "SELECT * FROM $navigation_table WHERE store_id = $storeId ORDER BY position";
        $data = $dbRead->query($query);
        $navigation = [];
        foreach($data->fetchAll() as $element){
            $navigation[] = $element;
        } 
        return $navigation;
    }

    /**
     * Recursively adds categories ad custom voices to top menu
     *
     * @param array $customNavigation
     * @param Varien_Data_Tree_Node $pNavigationNode
     * @param Mage_Page_Block_Html_Topmenu $menuBlock
     * @param bool $addTags
     */
    protected function _addVoicesToMenu($customNavigation, $NavigationNode, $menuBlock, $addTags = false)
    {

        foreach ($customNavigation as $voice) {

            $nodeId = 'category-node-' . $voice["id"];

            $tree = $NavigationNode->getTree();

            if($voice["catid"]){

                $category = Mage::getModel('catalog/category')->load($voice["catid"]);

                $categoryData = array(
                    'name' => $voice["name"],
                    'id' => $nodeId,
                    'url' => Mage::helper('catalog/category')->getCategoryUrl($category),
                    'is_active' => true,
                    'outerClasses' => $voice["outer_class"],
                    'innerClasses' => $voice["inner_class"]
                );

                $categoryNode = new Varien_Data_Tree_Node($categoryData, 'id', $tree, $NavigationNode);
                $NavigationNode->addChild($categoryNode);


                $subcategories = array();
                $subcat = explode(",",$category->getChildren());
                foreach($subcat as $sub){

                   $subcategories[] = Mage::getModel('catalog/category')->load($sub);

                }

                $this->_addCategoriesToMenu($subcategories, $categoryNode, $menuBlock, $addTags);


            } elseif($voice["block"]) {

                $block = $voice["block"]; 

                $voiceData = array(
                    'name' => $voice["name"],
                    'id' => $nodeId,
                    'url' => "#",
                    'is_active' => true,
                    'block_id' => $voice["block"],
                    'outerClasses' => $voice["outer_class"],
                    'innerClasses' => $voice["inner_class"]
                );
                $voiceNode = new Varien_Data_Tree_Node($voiceData, 'id', $tree, $NavigationNode);
                $NavigationNode->addChild($voiceNode);
            } else {

                $voiceData = array(
                    'name' => $voice["name"],
                    'id' => $nodeId,
                    'url' => $voice["url"],
                    'is_active' => true,
                    'urlTarget' => $voice["url_target"],
                    'outerClasses' => $voice["outer_class"],
                    'innerClasses' => $voice["inner_class"]
                );
                $voiceNode = new Varien_Data_Tree_Node($voiceData, 'id', $tree, $NavigationNode);
                $NavigationNode->addChild($voiceNode);
            }

        }
    }
}
