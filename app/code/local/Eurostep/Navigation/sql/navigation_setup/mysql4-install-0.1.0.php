<?php
$installer = $this;
$installer->startSetup();
$sql=<<<SQLTEXT
create table eurostep_navigation(id int not null auto_increment, store_id int(11), name  varchar(255), alias varchar(255), catid int(11), block int(11), url varchar(1024), name  varchar(255), outer_class  varchar(255), inner_class  varchar(255), url_target varchar(255), primary key(id));
		
SQLTEXT;

$installer->run($sql);

$installer->endSetup();
	 